import axios from 'axios'


const http = {
    get: axios.get,
    post: axios.post,
    delete: axios.delete,
    put: axios.put,
    api: 'http://localhost:8000/api'
}

export default http;