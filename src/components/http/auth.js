import http from './http'

async function register(data) {
    return http.post(http.api+"/register",data)
}
async function login(data) {
    return http.post(http.api+"/login",data)
}

const auth = {
    login,register
};
export default auth;