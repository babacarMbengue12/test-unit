import React,{Component} from 'react'
import Spinner from "../Spinner";
class Form extends Component{

    submit = async (e)=>{
        e.preventDefault()
        this.setState({loading: true})
        const errors = this.validate();
        if(Object.keys(errors).length > 0){
            this.setState({errors,loading: false})
        }
        else{
            this.setState({errors:{}})
            try {
                await this.doSubmit(this.state.data)
            }catch (e) {
                if(e.response && e.response.status === 400){
                    this.setState({errors: {...e.response.data},loading: false})
                }
            }
        }
    }
    change = ({target})=>{
        const {name,value} = target
        const data ={...this.state.data}
        data[name]  = value
        let errors = {...this.state.errors}
        const newError = this.validate(data)[name]
        if(newError) errors = {...errors,[name]: newError}
        else delete errors[name]

        this.setState({errors,data})
    }
    render(){
       return null;
    }
    _renderInput(name,label,type="text"){
        const value = this.state.data[name]
        const error = this.state.errors[name]
        return <div className="form-group">
            <label htmlFor="exampleInputEmail1">{label}</label>
            <input value={value} onChange={this.change} name={name} type={type} className={`form-control form-control-lg ${error && 'is-invalid'}`} id="exampleInputEmail1" aria-describedby="emailHelp"
                   placeholder={label} />
            {error && <small  className="form-text text-danger">{error}</small>}
        </div>
    }
    _renderButton(label,color="dark",type="submit"){
        const isValid = Object.keys(this.state.errors).length === 0
        return <button type={type} className={`btn btn-${color} ${!isValid ? 'disabled' : ''}`} disabled={!isValid ? 'disabled' : ''}>
            {this.state.loading && <Spinner size={1}/>}
            Submit
        </button>
    }
}

export default Form