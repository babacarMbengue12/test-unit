import React,{Component} from 'react'

class Spinner extends Component{


    render(){
        const {size} = this.props
        return <div className={`spinner-border ${size && "spinner-border-sm" }`} role="status">
            <span className="sr-only">Loading...</span>
        </div>

    }
}

Spinner.defaultProps = {
    size: false
}
export default Spinner