import React from 'react'
import validation from '../validation/validation'
import auth from "../http/auth";
import Form from "../common/forms/form";

class Login extends Form{
    state  = {
        data: {email: '',password: ''},
        loading: false,
        errors: {}
    };
    doSubmit = async (data)=>{
        await auth.login(data)
    };
    validate = (data=null)=>{
        const {email} = data ? data : this.state.data
        const errors = {}
        if(!validation.validateEmail(email)) errors.email = 'l\'email doit etre un email valide'
        return errors
    };
    render(){
        return <div className="container">
            <div className="row align-items-center justify-content-center">
                <div className="col-md-7">
                    <h1 className="text-center">Login</h1>
                    <form onSubmit={this.submit}>
                        {this._renderInput('email','Addresse Email')}
                        {this._renderInput('password','Password','password')}
                        {this._renderButton('Login')}
                    </form>
                </div>
            </div>
        </div>
    }
}

export default Login