import React,{Component} from 'react'
import validation from '../validation/validation'
import auth from "../http/auth";
import Form from "../common/forms/form";
class Register extends Form{

    state  = {
        data: {email: '',password: '',password_confirmation: ""},
        loading: false,
        errors: {}
    };
    doSubmit = async (e)=>{
        await auth.register(this.state.data)
    }
    validate = (data=null)=>{
        const {email,password,password_confirmation} = data ? data : this.state.data
        const errors = {}
        if(validation.validateLength(email,6))
            errors.email='l\'email doit avoir au minimum 6 caractères'
        if(validation.validateLength(password,8))
            errors.password='le mot de passe doit avoir au minimum 8 caractères'
        if(!errors.password && !validation.validatePassword(password))
            errors.password = 'le mot de passe doit avoir au minimum 1 chiffre et un majuscule'
        if(!errors.email && !validation.validateEmail(email)) errors.email = 'l\'email doit etre un email valide'
        if(password !== password_confirmation) errors.password_confirmation = 'les mots de passe doivent être les mêmes'
        return errors
    };
    render(){
        return <div className="container">
            <div className="row align-items-center justify-content-center">
                <div className="col-md-7">
                    <h1 className="text-center">Register To join us now</h1>
                    <form onSubmit={this.submit}>
                        {this._renderInput('email','Email address')}
                        {this._renderInput('password','Password','password')}
                        {this._renderInput('password_confirmation','Confirmer le mot de passe','password')}
                        {this._renderButton('Register')}
                    </form>
                </div>
            </div>
        </div>
    }
}

export default Register