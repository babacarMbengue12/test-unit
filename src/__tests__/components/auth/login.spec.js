import {wait} from '@testing-library/react'

import React from "react";
import Login from "../../../components/auth/Login";
jest.mock(".../../../components/http/http")
import http from "../../../components/http/http";




describe('<Login />',()=>{
    let login;
    beforeEach(()=>{
        login = mount(<Login />)
        http.post = jest.fn((data) => {
            return new Promise(resolve => setTimeout(()=> resolve(data)),2000)
        })
    })
    const simulateChange = (name,value)=>{
        login.find(`input[name="${name}"]`).simulate('change',getEvent(name,value))
    }
    const getEvent = (name,value)=>{
        return {target: {value,name}}
    }

    it("les champs doivent être obligatoirement rendus",()=>{
        expect(login.find('input')).toHaveLength(2);
    })
    it("les données doivent être sur le state\n",()=>{
        simulateChange('email','b@m.c');
        simulateChange('password','12345678');
        expect(Object.keys(login.state().data).length).toBe(2)
        expect(login.state().data.email).toBe('b@m.c')
        expect(login.state().data.password).toBe('12345678')
    })

    it("l'email doit etre un email valide", ()=>{
        simulateChange('email','babacar@mail');
        simulateChange('password','12345678');
         login.find('form').simulate('submit')
        expect(login.state().errors.email).toBe('l\'email doit etre un email valide')

        simulateChange('email','babacarmail');
         login.find('form').simulate('submit')
        expect(login.state().errors.email).toBeDefined()
        simulateChange('email','babacar@mail.com');
         login.find('form').simulate('submit')
        expect(login.state().errors.email).not.toBeDefined()

    })
    it("le button doivent être désactivé si les champs sont invalides",()=>{
        simulateChange('email','babacar@m');
        simulateChange('password','12345678');

        expect(login.find('button[type="submit"].disabled')).toHaveLength(1)

        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1babacar');

        expect(login.find('button[type="submit"].disabled')).toHaveLength(0)

    })
    it("le loader doit s’afficher en état de loading",async ()=>{
        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1babacar');
        expect(login.state().loading).toBe(false)
        expect(login.find('Spinner')).toHaveLength(0)
        await login.find('form').simulate('submit')
        expect(login.state().loading).toBe(true)
        expect(login.find('Spinner')).toHaveLength(1)

    })
    it("les messages d'erreurs doivent être affiché",()=>{
        simulateChange('email','babacar@m');
        simulateChange('password','12345678');
        login.find('form').simulate('submit')
        expect(login.find(`small.text-danger`)).toHaveLength(1)

        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1babacar');
        login.find('form').simulate('submit')
        expect(login.find(`small.text-danger`)).toHaveLength(0)

    })
    it("l’API doit être appelé avec les données du formulaires",async ()=>{
        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1babacar');
        await wait(() => login.find('form').simulate('submit'))
        expect(http.post).toHaveBeenCalledWith(http.api+"/login",login.state().data);

    })
    it("les erreurs en provenance de l’API doivent être affichés\n",async ()=>{
        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1babacar');
        http.post.mockRejectedValue({
            response: {data: {email: "email ou mot de passe incorrect"},status: 400}
        });
        await wait(() => login.find('form').simulate('submit'))
        expect(Object.keys(login.state().errors)).toHaveLength(1)
        expect(login.state().errors.email).toBe('email ou mot de passe incorrect')

    })


})