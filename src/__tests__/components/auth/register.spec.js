import React from "react";
import {wait} from '@testing-library/react'
import Register from "../../../components/auth/register";
jest.mock("../../../components/http/http")
import http from "../../../components/http/http";




describe('<Register />',()=>{
    let register;
    beforeEach(()=>{
        register = mount(<Register />)
        http.post = jest.fn((data) => {
            return new Promise(resolve => setTimeout(()=> resolve(data)),2000)
        })
    })
    const simulateChange = (name,value)=>{
        register.find(`input[name="${name}"]`).simulate('change',getEvent(name,value))
    }
    const getEvent = (name,value)=>{
        return {target: {value,name}}
    }

    it("les champs doivent être obligatoirement rendus",()=>{
        expect(register.find('input')).toHaveLength(3);
    })
    it("les données doivent être sur le state\n",()=>{
        simulateChange('email','b@m.c');
        simulateChange('password','12345678');
        simulateChange('password_confirmation','12345678');

        expect(Object.keys(register.state().data).length).toBe(3)
        expect(register.state().data.email).toBe('b@m.c')
        expect(register.state().data.password).toBe('12345678')
        expect(register.state().data.password_confirmation).toBe('12345678')
    })
    it("l'email doit avoir au minimum 6 caractères", ()=>{
        simulateChange('email','b@m.c');
        simulateChange('password','12345678');
        simulateChange('password_confirmation','12345678');

         register.find('form').simulate('submit')
        expect(register.state().errors.email).toBeDefined()
        expect(register.state().errors.email).toBe('l\'email doit avoir au minimum 6 caractères')

        simulateChange('email','babacar@mail.com');
         register.find('form').simulate('submit')
        expect(register.state().errors.email).not.toBeDefined()
    })

    it("le mot de passe doit avoir au minimum 1 chiffre et un majuscule", ()=>{
        simulateChange('email','babacar@mail.com');
        simulateChange('password','12345678');
        simulateChange('password_confirmation','12345678');
         register.find('form').simulate('submit')
        expect(register.state().errors.password).toBeDefined()
        expect(register.state().errors.password).toBe('le mot de passe doit avoir au minimum 1 chiffre et un majuscule')

        simulateChange('password','M1fkfkfkfk');
         register.find('form').simulate('submit')
        expect(register.state().errors.password).toBe(undefined)

        simulateChange('password','12345678B');
        register.find('form').simulate('submit')
        expect(register.state().errors.password).toBe(undefined)
    })
    it("les mots de passe doivent être les mêmes", ()=>{
        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1fkfkfkfk');
        simulateChange('password_confirmation','1234567');
        register.find('form').simulate('submit')
        expect(register.state().errors.password).not.toBeDefined()
        expect(register.state().errors.password_confirmation).toBeDefined()
        expect(register.state().errors.password_confirmation).toBe('les mots de passe doivent être les mêmes')

        simulateChange('password_confirmation','M1fkfkfkfk');
         register.find('form').simulate('submit')
        expect(register.state().errors.password_confirmation).toBe(undefined)
    })
    it("l'email doit etre un email valide", ()=>{
        simulateChange('email','babacar@mail');
        simulateChange('password','12345678');
        simulateChange('password_confirmation','12345678');
         register.find('form').simulate('submit')
        expect(register.state().errors.email).toBeDefined()
        expect(register.state().errors.email).toBe('l\'email doit etre un email valide')

        simulateChange('email','babacarmail');
         register.find('form').simulate('submit')
        expect(register.state().errors.email).toBeDefined()

        simulateChange('email','babacar@mail.com');
         register.find('form').simulate('submit')
        expect(register.state().errors.email).not.toBeDefined()

    })
    it("tous les champs doivent être valides", ()=>{
        simulateChange('email','babacar@m');
        simulateChange('password','12345678');
        simulateChange('password_confirmation','1234567');
         register.find('form').simulate('submit')
        expect(Object.keys(register.state().errors)).toHaveLength(3)

        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1babacar');
        simulateChange('password_confirmation','M1babacar');

         register.find('form').simulate('submit')
        expect(Object.keys(register.state().errors)).toHaveLength(0)

    })
    it("le button doivent être désactivé si les champs sont invalides",()=>{
        simulateChange('email','babacar@m');
        simulateChange('password','12345678');
        simulateChange('password_confirmation','1234567');

        expect(register.find('button[type="submit"].disabled')).toHaveLength(1)

        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1babacar');
        simulateChange('password_confirmation','M1babacar');

        expect(register.find('button[type="submit"].disabled')).toHaveLength(0)

    })
    it("l’API doit être appelé avec les données du formulaires",async ()=>{
        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1babacar');
        simulateChange('password_confirmation','M1babacar');


        await wait(() => register.find('form').simulate('submit'))
        expect(http.post).toHaveBeenCalledWith(http.api+"/register",register.state().data);

    })
    it("les erreurs en provenance de l’API doivent être affichés\n",async ()=>{
        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1babacar');
        simulateChange('password_confirmation','M1babacar');

        http.post.mockRejectedValue({
                response: {data: {email: "l'email existe deja"},status: 400}
            });
        await wait(() => register.find('form').simulate('submit'))

        expect(Object.keys(register.state().errors)).toHaveLength(1)
        expect(register.state().errors.email).toBe('l\'email existe deja')

    })
    it("le loader doit s’afficher en état de loading",async ()=>{
        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1babacar');
        simulateChange('password_confirmation','M1babacar');
        expect(register.state().loading).toBe(false)
        expect(register.find('Spinner')).toHaveLength(0)
        await register.find('form').simulate('submit')
        expect(register.state().loading).toBe(true)
        expect(register.find('Spinner')).toHaveLength(1)

    })
    it("les messages d'erreurs doivent être affiché",()=>{
        simulateChange('email','babacar@m');
        simulateChange('password','12345678');
        simulateChange('password_confirmation','123456');
        register.find('form').simulate('submit')
        expect(register.find(`small.text-danger`)).toHaveLength(3)

        simulateChange('email','babacar@mail.com');
        simulateChange('password','M1babacar');
        simulateChange('password_confirmation','M1babacar');
        register.find('form').simulate('submit')
        expect(register.find(`small.text-danger`)).toHaveLength(0)

    })



})