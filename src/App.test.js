import React from 'react';
import {MemoryRouter} from 'react-router'
import { mount } from 'enzyme';
import App from './App';

test('renders learn react link', () => {
  const app =  mount(<MemoryRouter initialEntries={['/']}><App /></MemoryRouter>);
  expect(app.find('App')).toHaveLength(1);
});
