import * as Sentry from '@sentry/browser';

export function init() {
    if(process.env.NODE_ENV === 'production'){
        Sentry.init({dsn: "https://7d4651a483eb40bd9e2a1470f7c4652b@sentry.io/2062841"});
    }
}