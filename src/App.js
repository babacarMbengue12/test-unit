import React from 'react';
import 'jquery/dist/jquery'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap'
import './App.css';
import Register from "./components/auth/register";
import {NavLink, Link, Route} from "react-router-dom";
import Login from "./components/auth/Login";
function Home(props) {
    return <div className="jumbotron">
        <h1>Unit testing</h1>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aspernatur hic id iure, maxime, minima neque nisi nostrum odio officia recusandae reprehenderit repudiandae tenetur! Consequatur in minus natus neque odit.
        </p>
    </div>;
}
function Navbar(props) {
    return <div className="navbar navbar-dark bg-dark navbar-expand-md">
        <div className="container">
            <Link to="/" className="navbar-brand">Unit Testing</Link>
            <button className="navbar-toggler" data-toggle="collapse" data-target="#menu">
                <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse justify-content-end" id="menu">
                <ul className="nav navbar-nav">
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/register">Register</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/login">Login</NavLink>
                    </li>
                </ul>
            </div>
        </div>
    </div>
}
function App() {

    return (
    <main className="min-vh-100">
        <Navbar/>
        <Route path="/register" exact component={Register} />
        <Route path="/login" exact component={Login} />
        <Route path="/" exact component={Home} />
    </main>
  );
}

export default App;
